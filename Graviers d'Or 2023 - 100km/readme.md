## Graviers d'Or 4

- 1er Octobre 2023
- Dijon et alentours
- 101km, 950D+

Organisé par Scops (https://scops.cc)

https://www.komoot.com/fr-fr/tour/1272439903

### Roadbook
- Km 23 Point d’eau
- Km 33 Nombreux commerces
- Km 41 Descente pierreuse
- Km 44 Point d’eau
- Km 50 Point d’eau
- Km 58,6 Boulangerie ouverte jusqu’à 13h
- Km 58,6 Supermarché ouvert jusqu’à 12h30
- Km 67 Point d’eau (derrière la salle polyvalente, en contrebas)
- Km 88 Traversée de Dijon (nombreux commerces si besoin)
