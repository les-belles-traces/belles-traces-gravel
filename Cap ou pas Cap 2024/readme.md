# Cap ou pas Cap Gravel 2024

Week-end de la Pentecôte 2024 (17-20 mai 2024)

Organisé par le Cyclo Club de Montebourg - Saint Germain de Tournebut (<https://www.cyclo-club-montebourg-saint-germain-de-tournebut.com/>)

<https://www.cyclo-club-montebourg-saint-germain-de-tournebut.com/longue-distance/2024/cap-ou-pas-cap-gravel-vtt/>

## Quelques chiffres

- Distance : 400km
- D + :  4200m
- Délai :  3.5 jours maxi
- % de chemin : à affiner, 60% environ
- Le point culminant : 180m
- Nb de personne maxi : 198 (dont 100 maxi sur chacun des départs du vendredi et samedi)
- Nb de poussage : sans doute quelques passages en montée un peu difficile (mais pas long) pour de nombreux participants
- BPF : 2
- Le nombre de wahou : beaucoup

![carte de la Cap ou pas Cap Gravel 2024](map.jpg "carte de la Cap ou pas Cap Gravel 2024")